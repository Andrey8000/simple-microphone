package crocodile8008.simplemicrophone;

import android.support.v4.app.Fragment;
import android.test.ActivityInstrumentationTestCase2;

import crocodile8008.simplemicrophone.view.MainFragment;

/**
 * Created by Andrey Riyk
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2 {

    //region Constructors

    public MainActivityTest() {
        super(MainActivity.class);
    }

    public MainActivityTest(Class activityClass) {
        super(activityClass);
    }

    public MainActivityTest(String pkg, Class activityClass) {
        super(pkg, activityClass);
    }

    //endregion

    private MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }

    public void testResumedState() {
        assertTrue(getMainActivity().isResumedState());
    }

    public void testLoadsFragment() {
        Fragment fragment = getMainActivity().getSupportFragmentManager()
                .findFragmentByTag("mainFragment");
        assertTrue(fragment instanceof MainFragment);
    }

    public void testRecordButtonNotNull() {
        assertNotNull(getMainActivity().findViewById(R.id.button));
    }

    //TODO need Robotium, but 'testCompile' not work with gradle plugin experimental 0.1.0
}
