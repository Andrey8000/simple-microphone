package crocodile8008.simplemicrophone.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import crocodile8008.simplemicrophone.helpers.Lo;

/**
 * Created by Andrey Riyk
 */
public class ScrollableRecyclerView extends RecyclerView {

    @Nullable
    private ScrollListener scrollListener;

    private float lastY;
    private boolean wasActionUp;

    //region constructors

    public ScrollableRecyclerView(Context context) {
        super(context);
    }

    public ScrollableRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ScrollableRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    
    //endregion

    public void setScrollListener(@Nullable ScrollListener scrollListener) {
        this.scrollListener = scrollListener;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            lastY = ev.getY();
            wasActionUp = false;
        }
        else if (ev.getAction() == MotionEvent.ACTION_MOVE) {
            boolean isFreshInstance = lastY == 0;
            // if someone steal ACTION_DOWN
            if (wasActionUp) {
                lastY = ev.getY();
                wasActionUp = false;
            }
            if (isFreshInstance) {
                lastY = ev.getY();
                return super.onTouchEvent(ev);
            }

            if (scrollListener != null) {
                float diff = lastY - ev.getY();
                boolean isScrollHandled = scrollListener.onScroll(diff);
                if (isScrollHandled) {
                    scrollBy(0, (int) (diff / 2));
                    return true;
                } else {
                    lastY = ev.getY();
                    return super.onTouchEvent(ev);
                }
            }
            lastY = ev.getY();
        }
        else if (ev.getAction() == MotionEvent.ACTION_UP
                || ev.getAction() == MotionEvent.ACTION_CANCEL) {
            wasActionUp = true;
            if (scrollListener != null) {
                scrollListener.onTapUp();
            }
        }
        return super.onTouchEvent(ev);
    }

    //TODO it's very roughly
    public boolean isFilledVertical() {
        if (getChildCount() == 0) {
            return false;
        }
        View view = getChildAt(0);
        int itemsCnt = getAdapter().getItemCount();
        return view.getHeight() * itemsCnt > getHeight() * 0.8;
    }

    public interface ScrollListener {
        boolean onScroll(float diffY);
        void onTapUp();
    }
}
