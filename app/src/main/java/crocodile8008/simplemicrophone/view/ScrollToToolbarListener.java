package crocodile8008.simplemicrophone.view;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Interpolator;

import java.lang.ref.WeakReference;

import crocodile8008.simplemicrophone.helpers.Lo;

/**
 * Created by Andrey Riyk
 */
public class ScrollToToolbarListener implements ScrollableRecyclerView.ScrollListener {

    private static final int DIRECTION_EMPTY = 0;
    private static final int DIRECTION_SCROLL_DOWN = 1;
    private static final int DIRECTION_SCROLL_UP = -1;

    private final WeakReference<Toolbar> weakToolbar;
    private final WeakReference<View> weakParentView;
    private final WeakReference<ScrollableRecyclerView> weakRecyclerView;

    private int currentDirection = DIRECTION_EMPTY;

    public ScrollToToolbarListener(Toolbar toolbar, View parentView, ScrollableRecyclerView recyclerView) {
        weakToolbar = new WeakReference<>(toolbar);
        weakParentView = new WeakReference<>(parentView);
        weakRecyclerView = new WeakReference<>(recyclerView);
    }

    @Override
    public boolean onScroll(float dy) {
        Toolbar toolbar = weakToolbar.get();
        View parentView = weakParentView.get();
        ScrollableRecyclerView scrollableRecyclerView = weakRecyclerView.get();
        if (toolbar == null || parentView == null || scrollableRecyclerView == null) {
            return false;
        }

        currentDirection = dy > 0 ? DIRECTION_SCROLL_DOWN
                        : dy < 0 ? DIRECTION_SCROLL_UP
                        : 0;

        if (dy > 0 && scrollableRecyclerView.isFilledVertical()) {
            if (toolbar.getY() > -toolbar.getHeight()) {
                float targetY = toolbar.getY() - dy;
                float finalY = -toolbar.getHeight();
                if (targetY < finalY) {
                    targetY = finalY;
                }
                toolbar.setY(targetY);
                parentView.setY(targetY + toolbar.getHeight());
                return true;
            }

        } else if (dy < 0) {
            if (toolbar.getY() < 0) {
                float targetY = toolbar.getY() - dy;
                if (targetY > 0) {
                    targetY = 0;
                }
                toolbar.setY(targetY);
                parentView.setY(targetY + toolbar.getHeight());
                return true;
            }
        }
        return false;
    }

    @Override
    public void onTapUp() {
        appearOrDisappearToolbarAfterScroll();
        currentDirection = DIRECTION_EMPTY;
    }

    private void appearOrDisappearToolbarAfterScroll() {
        final Toolbar toolbar = weakToolbar.get();
        if (toolbar == null) {
            return;
        }

        float toolbarY = toolbar.getY();
        if (toolbarY > -toolbar.getHeight() / 2 && toolbarY != 0) {
            moveToolbarAnimated(toolbar, DIRECTION_SCROLL_DOWN);
        } else if (toolbarY <= -toolbar.getHeight() / 2 && toolbarY != -toolbar.getHeight()) {
            moveToolbarAnimated(toolbar, DIRECTION_SCROLL_UP);
        }
    }

    private void moveToolbarAnimated(@NonNull final Toolbar toolbar, int direction) {
        final View parentView = weakParentView.get();
        final RecyclerView scrollableRecyclerView = weakRecyclerView.get();
        if (parentView == null || scrollableRecyclerView == null) {
            return;
        }

        ObjectAnimator anim1 = ObjectAnimator.ofFloat(toolbar, "y",
                direction == DIRECTION_SCROLL_DOWN ? 0 : -toolbar.getHeight());
        ObjectAnimator anim2 = ObjectAnimator.ofFloat(parentView, "y",
                direction == DIRECTION_SCROLL_DOWN ? toolbar.getHeight() : 0);
        AnimatorSet animSetXY = new AnimatorSet();
        animSetXY.playTogether(anim1, anim2);
        animSetXY.setDuration(200);
        animSetXY.start();
    }

}