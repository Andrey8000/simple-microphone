package crocodile8008.simplemicrophone.view;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.TextView;

import java.io.File;
import java.util.List;

import crocodile8008.simplemicrophone.App;
import crocodile8008.simplemicrophone.helpers.Lo;
import crocodile8008.simplemicrophone.utils.MicFileUtils;
import crocodile8008.simplemicrophone.utils.ThreadHelper;
import crocodile8008.simplemicrophone.utils.record.Recorder;
import crocodile8008.simplemicrophone.R;
import crocodile8008.simplemicrophone.view.helpers.AdapterUtils;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Andrey Riyk
 */
public class MainFragment extends Fragment {

    private final Recorder recorder;
    private final MicFileUtils micFileUtils;
    private final AdapterUtils adapterUtils;

    @Nullable
    private View.OnClickListener externalRecordClickListener;
    @Nullable
    private FilesRecyclerAdapter.ItemClickListener externalFilesListClickListener;
    @Nullable
    private FilesRecyclerAdapter.LongClickListener externalFilesLongClickListener;

    private Drawable micOnDrawable;
    private Drawable micOffDrawable;
    private Button buttonRecord;
    private TextView textViewCount;
    private ScrollableRecyclerView recyclerView;
    private FilesRecyclerAdapter adapter;

    private int savedFirstVisiblePosition;

    @Nullable
    private Subscription subscriptionUpdAdapter;

    public MainFragment() {
        super();
        recorder = App.getInstance().getRecorder();
        micFileUtils = new MicFileUtils();
        adapterUtils = new AdapterUtils(micFileUtils);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        textViewCount = (TextView) rootView.findViewById(R.id.textViewCount);
        updateDisplayedFilesCount();

        initDrawables();

        setupRecordButton(rootView);
        updateRecordBtnState();

        setupRecyclerView(rootView);
        updateAdapterDataAsync();

        return rootView;
    }

    @Override
    public void onDestroyView() {
        unsubscribePreviousAdapterDataUpd();
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        if (layoutManager != null) {
            savedFirstVisiblePosition = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
        }
        super.onDestroyView();
    }

    private void initDrawables() {
        if (micOnDrawable == null) {
            micOnDrawable = getResources().getDrawable(R.drawable.microphone_small_on_air);
        }
        if (micOffDrawable == null) {
            micOffDrawable = getResources().getDrawable(R.drawable.microphone_small);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateToolbarAndLayoutPosition();
        recyclerView.setScrollListener(
                new ScrollToToolbarListener(getToolbar(), getFragmentParentView(), recyclerView));
    }

    private void updateToolbarAndLayoutPosition() {
        final Toolbar toolbar = getToolbar();
        final View parentLayout = getFragmentParentView();
        if (toolbar == null || parentLayout == null) {
            Lo.e("updateToolbarAndLayoutPosition failed");
            return;
        }
        if (toolbar.getY() == 0) {
            updateParentLayoutPosition(toolbar, parentLayout);
        } else if (toolbar.getY() != -toolbar.getHeight() && toolbar.getY() < - toolbar.getHeight() / 2) {
            toolbar.setY(-toolbar.getHeight());
            parentLayout.setY(0);
        } else if (toolbar.getY() != 0 && toolbar.getY() > - toolbar.getHeight() / 2) {
            toolbar.setY(0);
            parentLayout.setY(toolbar.getHeight());
        }
    }

    private void updateParentLayoutPosition(final Toolbar toolbar, final View parentLayout) {
        if (toolbar.getHeight() == 0) {
            toolbar.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    parentLayout.setY(toolbar.getMeasuredHeight());
                    toolbar.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
        } else {
            parentLayout.setY(toolbar.getMeasuredHeight());
        }
    }

    private void setupRecordButton(View rootView) {
        buttonRecord = (Button) rootView.findViewById(R.id.button);
        buttonRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (externalRecordClickListener != null) {
                    externalRecordClickListener.onClick(view);
                }
                updateRecordBtnState();
            }
        });
    }

    private void setupRecyclerView(View rootView) {
        recyclerView = (ScrollableRecyclerView) rootView.findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new FilesRecyclerAdapter();
        recyclerView.setAdapter(adapter);
        adapter.setClickListener(new FilesRecyclerAdapter.ItemClickListener() {
            @Override
            public void onClick(File file, int position) {
                if (externalFilesListClickListener != null) {
                    externalFilesListClickListener.onClick(file, position);
                }
            }
        });
        adapter.setLongClickListener(new FilesRecyclerAdapter.LongClickListener() {
            @Override
            public void onLongClick(final File file, int position) {
                if (externalFilesLongClickListener != null) {
                    externalFilesLongClickListener.onLongClick(file, position);
                }
            }
        });
    }

    @Nullable
    private Toolbar getToolbar() {
        if (isDetached()) {
            return null;
        }
        View toolbar = getActivity().findViewById(R.id.toolbar);
        if (toolbar == null || !(toolbar instanceof Toolbar)){
            return null;
        }
        return (Toolbar) toolbar;
    }

    private View getFragmentParentView() {
        if (isDetached() || getView() == null || getView().getParent() == null) {
            return null;
        }
        return (View) getView().getParent();
    }

    public void handleFilesInDirChanges() {
        if (isDetached()) {
            return;
        }
        updateAdapterDataAsync();
    }

    private void updateAdapterDataAsync() {
        unsubscribePreviousAdapterDataUpd();
        subscriptionUpdAdapter =
            Observable
                    .create(new Observable.OnSubscribe<List<FilesRecyclerAdapter.Item>>() {
                        @Override
                        public void call(Subscriber<? super List<FilesRecyclerAdapter.Item>> subscriber) {
                            List<FilesRecyclerAdapter.Item> items = adapterUtils.getItemsToAdapter();
                            subscriber.onNext(items);
                            subscriber.onCompleted();
                        }
                    })
                    .subscribeOn(Schedulers.from(ThreadHelper.COMMON_EXECUTOR_POOL))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<List<FilesRecyclerAdapter.Item>>() {
                        @Override
                        public void call(List<FilesRecyclerAdapter.Item> items) {
                            adapter.updateContent(items);
                            updateDisplayedFilesCount();
                            subscriptionUpdAdapter = null;
                            restoreScrollPositionIfNeeded();
                        }
                    });
    }

    private void unsubscribePreviousAdapterDataUpd() {
        if (subscriptionUpdAdapter != null && !subscriptionUpdAdapter.isUnsubscribed()) {
            subscriptionUpdAdapter.unsubscribe();
            subscriptionUpdAdapter = null;
        }
    }

    private void restoreScrollPositionIfNeeded() {
        if (savedFirstVisiblePosition != 0) {
            recyclerView.scrollToPosition(savedFirstVisiblePosition);
            savedFirstVisiblePosition = 0;
        }
    }

    public void redrawAdapterView() {
        if (isDetached()) {
            return;
        }
        adapter.notifyDataSetChanged();
    }

    private void updateDisplayedFilesCount() {
        textViewCount.setText(micFileUtils.getFilesCntInWorkDir() + " " + getString(R.string.files));
    }

    public void setFilesListClickListener(FilesRecyclerAdapter.ItemClickListener listener) {
        externalFilesListClickListener = listener;
    }

    public void setFilesLongClickListener(FilesRecyclerAdapter.LongClickListener listener) {
        externalFilesLongClickListener = listener;
    }

    public void setRecordClickListener(View.OnClickListener listener) {
        externalRecordClickListener = listener;
    }

    private void updateRecordBtnState() {
        if (recorder.isRecordingNow()) {
            //TODO cache values
            buttonRecord.setText(getString(R.string.stop_record));
            buttonRecord.setTextColor(getResources().getColor(R.color.red));
            buttonRecord.setCompoundDrawablesWithIntrinsicBounds(micOnDrawable, null, null, null);
        } else {
            buttonRecord.setText(getString(R.string.start_record));
            buttonRecord.setTextColor(getResources().getColor(R.color.green));
            buttonRecord.setCompoundDrawablesWithIntrinsicBounds(micOffDrawable, null, null, null);
        }
    }

}
