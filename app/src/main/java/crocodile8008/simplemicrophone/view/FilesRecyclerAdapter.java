package crocodile8008.simplemicrophone.view;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import crocodile8008.simplemicrophone.App;
import crocodile8008.simplemicrophone.R;
import crocodile8008.simplemicrophone.utils.MicFileUtils;
import crocodile8008.simplemicrophone.utils.play.RecordPlayer;
import crocodile8008.simplemicrophone.view.helpers.FileDurationFetcher;

import rx.Subscription;

/**
 * Created by Andrey Riyk
 */
public class FilesRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM_FILE = 0;
    private static final int ITEM_SEPARATOR = 1;

    @Nullable
    private ItemClickListener externalClickListener;
    @Nullable
    private LongClickListener externalLongClickListener;

    private List<Item> itemList;
    private final RecordPlayer recordPlayer;
    private Drawable volumeDrawable;
    private Drawable volumeDrawableEmpty;

    public FilesRecyclerAdapter() {
        itemList = new ArrayList<>();
        recordPlayer = App.getInstance().getRecordPlayer();

        Resources resources = App.getInstance().getResources();
        volumeDrawable = resources.getDrawable(R.drawable.ic_volume_up_black_24dp);
        volumeDrawableEmpty = resources.getDrawable(R.drawable.ic_volume_up_black_24dp_empty);
    }

    public void updateContent(List<Item> filesList) {
        this.itemList = filesList;
        checkListNonNull();
        notifyDataSetChanged();
    }

    private void checkListNonNull() {
        if (itemList == null) {
            itemList = new ArrayList<>();
        }
    }

    public void setClickListener(ItemClickListener externalClickListener) {
        this.externalClickListener = externalClickListener;
    }

    public void setLongClickListener(LongClickListener externalLongClickListener) {
        this.externalLongClickListener = externalLongClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position).isFile() ? ITEM_FILE : ITEM_SEPARATOR;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_FILE:
                return new FileViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.files_list_item, parent, false));

            case ITEM_SEPARATOR:
                return new SeparatorViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.files_list_item_separator, parent, false));

            default:
                throw new IllegalStateException("Undefined item type");
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder incHolder, final int position) {
        switch (getItemViewType(position)) {

            case ITEM_FILE:
                final FileViewHolder holder = (FileViewHolder) incHolder;
                holder.stopInfoFetching();
                holder.textViewDuration.setText("");

                final File file = itemList.get(position).file;

                holder.textViewName.setText(file.getName());
                setLeftDrawableIfFileIsSound(holder.textViewName, file);
                holder.baseView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        handleClick(file, position, holder);
                    }
                });
                holder.baseView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        handleLongClick(file, position);
                        return false;
                    }
                });

                String filePath = file.getAbsolutePath();
                if (!recordPlayer.isPlayingNow() || !filePath.equals(recordPlayer.getLastPayedFilePath())) {
                    holder.seekBar.setVisibility(View.GONE);
                } else {
                    setSeekBarActiveForPlaying(holder.seekBar);
                }

                holder.fetchFileInfoAsync(file);
                break;


            case ITEM_SEPARATOR:
                final SeparatorViewHolder separatorHolder = (SeparatorViewHolder) incHolder;
                separatorHolder.textViewName.setText(itemList.get(position).separatorName);
                break;

            default:
                throw new IllegalStateException("Undefined item type");
        }
    }

    private void setLeftDrawableIfFileIsSound(TextView textView, File file) {
        textView.setCompoundDrawablesWithIntrinsicBounds(
                MicFileUtils.isFileSound(file)? volumeDrawable : volumeDrawableEmpty, null, null, null);
    }

    private void handleClick(File file, int position, FileViewHolder holder) {
        if (externalClickListener != null) {
            externalClickListener.onClick(file, position);
            if (recordPlayer.isPlayingNow()) {
                setSeekBarActiveForPlaying(holder.seekBar);
            } else {
                holder.seekBar.setVisibility(View.GONE);
                recordPlayer.setViewForProgress(null);
            }
        }
    }

    private void handleLongClick(File file, int position) {
        if (externalLongClickListener != null) {
            externalLongClickListener.onLongClick(file, position);
        }
    }

    private void setSeekBarActiveForPlaying(SeekBar seekBar) {
        seekBar.setVisibility(View.VISIBLE);
        seekBar.setAlpha(0f);
        seekBar.animate().alpha(1f).setDuration(150).start();
        recordPlayer.setViewForProgress(seekBar);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                recordPlayer.seekTo(seekBar.getProgress());
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    static class FileViewHolder extends RecyclerView.ViewHolder {
        public final View baseView;
        public final TextView textViewName;
        public final TextView textViewDuration;
        public final SeekBar seekBar;

        @Nullable
        private Subscription fileInfoSubscription;

        public FileViewHolder(View view) {
            super(view);
            baseView = view;
            textViewName = (TextView) view.findViewById(R.id.textView2);
            textViewDuration = (TextView) view.findViewById(R.id.textView3);
            seekBar = (SeekBar) view.findViewById(R.id.progressBar);
        }

        void fetchFileInfoAsync(@NonNull final File file) {
            fileInfoSubscription = FileDurationFetcher.fetchDurationToTextViewAsync(file, textViewDuration);
        }

        void stopInfoFetching() {
            if (fileInfoSubscription != null && !fileInfoSubscription.isUnsubscribed()) {
                fileInfoSubscription.unsubscribe();
            }
        }
    }

    static class SeparatorViewHolder extends RecyclerView.ViewHolder {
        public final TextView textViewName;

        public SeparatorViewHolder(View view) {
            super(view);
            textViewName = (TextView) view.findViewById(R.id.textView2);
        }
    }

    public interface ItemClickListener {
        void onClick(File file, int position);
    }

    public interface LongClickListener {
        void onLongClick(File file, int position);
    }

    public static class Item {
        @Nullable
        public final File file;
        @Nullable
        public final String separatorName;

        public Item(@Nullable File file, @Nullable String separatorName) {
            this.file = file;
            this.separatorName = separatorName;
        }

        public boolean isFile() {
            return file != null;
        }

        public boolean isSeparator() {
            return separatorName != null;
        }
    }
}