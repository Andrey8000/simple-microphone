package crocodile8008.simplemicrophone.view.helpers;

import android.media.MediaMetadataRetriever;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.LruCache;
import android.widget.TextView;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Andrey Riyk
 */
public class FileDurationFetcher {

    private static final Executor executor = Executors.newFixedThreadPool(2);
    private static final FuncMillsToTime funcMillsToTime = new FuncMillsToTime();
    //TODO need notification about file renaming
    private static final LruCache<String, String> cache = new LruCache<>(100);

    /**
     * Fetch file duration and assign it in to given TextView (wrapped in to WeakReference)
     */
    @Nullable
    public static Subscription fetchDurationToTextViewAsync(@NonNull final File file,
                                                            @NonNull TextView textView) {
        String valueFromCache = cache.get(file.getName());
        if (valueFromCache != null) {
            textView.setText(valueFromCache);
            return null;
        }

        final WeakReference<TextView> weakTextView = new WeakReference<>(textView);
        return Observable
                        .create(new DurationFromFileFetcher(file))
                        .map(funcMillsToTime)
                        .map(new FuncPutToCache(file.getName()))
                        .subscribeOn(Schedulers.from(executor))
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new TextViewAssigner(weakTextView));
    }

    private static long getDuration(@NonNull File file) {
        if (!file.exists() || file.isDirectory()) {
            return -1;
        }
        MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
        try {
            metaRetriever.setDataSource(file.getAbsolutePath());
        } catch (RuntimeException fileWithoutMetadataException) {
            return -1;
        }
        String durationString = metaRetriever.extractMetadata(
                MediaMetadataRetriever.METADATA_KEY_DURATION);
        long duration = Long.parseLong(durationString);
        metaRetriever.release();

        return duration;
    }

    @NonNull
    private static String millsToTime(long timeMills) {
        if (timeMills <= 0) {
            return "";
        }

        long second = (timeMills / 1000) % 60;
        long minute = (timeMills / (1000 * 60)) % 60;
        long hour = (timeMills / (1000 * 60 * 60)) % 24;

        return hour > 0 ? String.format("%02d:%02d:%02d", hour, minute, second)
                : String.format("%02d:%02d", minute, second);
    }

    //region Classes

    static class DurationFromFileFetcher implements Observable.OnSubscribe<Long> {
        @NonNull
        private final File file;

        public DurationFromFileFetcher(@NonNull File file) {
            this.file = file;
        }

        public void call(Subscriber<? super Long> subscriber) {
            Long duration = getDuration(file);
            subscriber.onNext(duration);
            subscriber.onCompleted();
        }
    }

    static class FuncMillsToTime implements Func1<Long, String> {
        @Override
        public String call(Long aLong) {
            return millsToTime(aLong);
        }
    }

    static class FuncPutToCache implements Func1<String, String> {
        final String fileName;

        public FuncPutToCache(String fileName) {
            this.fileName = fileName;
        }

        @Override
        public String call(String arg) {
            cache.put(fileName, arg);
            return arg;
        }
    }

    static class TextViewAssigner implements Action1<String> {
        final WeakReference<TextView> weakTextView;

        public TextViewAssigner(WeakReference<TextView> weakTextView) {
            this.weakTextView = weakTextView;
        }

        @Override
        public void call(String duration) {
            TextView restoredTextView = weakTextView.get();
            if (restoredTextView != null) {
                restoredTextView.setText(duration + "");
            }
        }
    }

    //endregion
}
