package crocodile8008.simplemicrophone.view.helpers;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.view.View;

import com.afollestad.materialdialogs.AlertDialogWrapper;
import com.afollestad.materialdialogs.MaterialDialog;

import java.io.File;

import crocodile8008.simplemicrophone.R;
import crocodile8008.simplemicrophone.utils.record.Recorder;

/**
 * Created by Andrey Riyk
 */
public class DialogHelper {

    private final Activity activity;
    private final Recorder recorder;
    private final FilesChangesNotify filesChangesNotify;

    public DialogHelper(Activity activity, Recorder recorder, FilesChangesNotify filesChangesNotify) {
        this.activity = activity;
        this.recorder = recorder;
        this.filesChangesNotify = filesChangesNotify;
    }

    public void showLongClickChoice(@Nullable final File file) {
        if (file == null) {
            return;
        }
        new MaterialDialog.Builder(activity)
                .title(file.getName())
                .items(R.array.file_long_click_chooser)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        switch (which) {
                            case 0:
                                shareFile(file);
                                break;
                            case 1:
                                showRenameDialog(file);
                                break;
                            case 2:
                                showDeleteDialog(file);
                                break;
                            default:
                                break;
                        }
                    }
                })
                .show();
    }

    private void shareFile(@NonNull final File file) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setType("audio/mp3");
        intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + file.getAbsolutePath()));
        try {
            activity.startActivity(Intent.createChooser(intent, file.getName()));
        } catch (android.content.ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void showRenameDialog(@NonNull final File file) {
        final String ourFileExt = recorder.getFileExtension();
        final boolean hasOurExtension = file.getName().contains(ourFileExt);

        new MaterialDialog.Builder(activity)
                .title(activity.getString(R.string.rename_file))
                .inputType(InputType.TYPE_CLASS_TEXT)
                .input("", file.getName(), false, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        String newName = input.toString();
                        if (hasOurExtension && !newName.contains(ourFileExt)) {
                            newName += ourFileExt;
                        }
                        String newPath = file.getParent() + "/" + newName;
                        file.renameTo(new File(newPath));
                        filesChangesNotify.onFilesChanged();
                    }
                })
                .show();
    }

    private void showDeleteDialog(@NonNull final File file) {
        new AlertDialogWrapper.Builder(activity)
                .setTitle(activity.getString(R.string.del_file_question))
                .setMessage(file.getName())
                .setPositiveButton(activity.getString(R.string.del),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                file.delete();
                                filesChangesNotify.onFilesChanged();
                            }
                        })
                .setNegativeButton(activity.getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                .show();
    }

    public interface FilesChangesNotify {
        void onFilesChanged();
    }
}
