package crocodile8008.simplemicrophone.view.helpers;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import crocodile8008.simplemicrophone.utils.MicFileUtils;
import crocodile8008.simplemicrophone.view.FilesRecyclerAdapter;

/**
 * Created by Andrey Riyk
 */
public class AdapterUtils {

    @SuppressLint("SimpleDateFormat")
    private static final SimpleDateFormat formatter = new SimpleDateFormat("dd.MM");

    private final MicFileUtils micFileUtils;

    public AdapterUtils(MicFileUtils micFileUtils) {
        this.micFileUtils = micFileUtils;
    }

    /**
     * Read file list and convert it to list with separators for adapter
     * <br>
     * (4-30 ms with 25 files on nexus 5)
     */
    public List<FilesRecyclerAdapter.Item> getItemsToAdapter() {
        List<MicFileUtils.FileContainer> fileList = micFileUtils.getFilesListInWorkDir();
        List<FilesRecyclerAdapter.Item> itemList = new ArrayList<>();
        if (fileList.size() == 0) {
            return itemList;
        }

        Calendar lastDate = Calendar.getInstance();
        lastDate.setTimeInMillis(0);
        Calendar newDate = Calendar.getInstance();

        for (MicFileUtils.FileContainer fileContainer : fileList) {
            newDate.setTimeInMillis(fileContainer.getModifyTime());
            if (fileContainer.getModifyTime() > 0 && isDifferentDates(lastDate, newDate)) {

                itemList.add(new FilesRecyclerAdapter.Item(null, formatter.format(newDate.getTime())));
            }
            itemList.add(new FilesRecyclerAdapter.Item(fileContainer.file, null));
            lastDate.setTimeInMillis(newDate.getTimeInMillis());
        }

        return itemList;
    }

    private boolean isDifferentDates(Calendar newDate, Calendar lastDate) {
        return newDate.get(Calendar.DATE) != lastDate.get(Calendar.DATE)
            || newDate.get(Calendar.MONTH) != lastDate.get(Calendar.MONTH);
    }


}
