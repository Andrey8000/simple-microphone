package crocodile8008.simplemicrophone;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.MessageQueue;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Andrey Riyk
 *
 * Note: this is experimental class, <br>
 * {@link #onFinish() onFinish()} method is not really needed <br>
 * with manually handling of orientation changes (05.09.2015)
 */
public class AbsStateActivity extends AppCompatActivity {

    private static final long CHECK_INTERVAL = 700;
    private static boolean hasCreatedInstance;

    private final Handler handler = new Handler();
    private final CheckNewOnCreateTask checkNewOnCreateTask = new CheckNewOnCreateTask();

    private boolean isResumed;

    public boolean isResumedState() {
        return isResumed;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        hasCreatedInstance = true;
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            onFirstCreate();
        }
    }

    /**
     * Called when activity created at first time
     */
    public void onFirstCreate() {

    }

    @Override
    public void onResume() {
        super.onResume();
        isResumed = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        isResumed = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hasCreatedInstance = false;
        handler.postDelayed(checkNewOnCreateTask, CHECK_INTERVAL);
    }

    /**
     * Called if there is no more onCreate() after onDestroy()
     */
    public void onFinish() {

    }

    private class CheckNewOnCreateTask implements Runnable {
        @Override
        public void run() {
            if (!hasCreatedInstance) {
                onFinish();
            } else {
                // until this moment we have link to destroyed activity
                // so cause gc cleaning
                Looper.myQueue().addIdleHandler(new MessageQueue.IdleHandler() {
                    @Override
                    public boolean queueIdle() {
                        System.gc();
                        return false;
                    }
                });
            }
        }
    }
}
