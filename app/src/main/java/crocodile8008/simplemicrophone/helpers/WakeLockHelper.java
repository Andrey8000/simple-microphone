package crocodile8008.simplemicrophone.helpers;

import android.content.Context;
import android.os.PowerManager;

import crocodile8008.simplemicrophone.App;

/**
 * Created by Andrey Riyk
 */
public class WakeLockHelper {

    private static PowerManager.WakeLock wakeLock;

    public static void acquire() {
        if (wakeLock == null) {
            PowerManager powerManager = (PowerManager) App.getInstance()
                    .getSystemService(Context.POWER_SERVICE);
            wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "WakelockTag");
        }
        if (!wakeLock.isHeld()) {
            wakeLock.acquire();
            Lo.v("wakeLock.acquire");
        }
    }

    public static void release() {
        if (wakeLock != null) {
            if (wakeLock.isHeld()) {
                wakeLock.release();
                Lo.v("wakeLock.release");
            }
            wakeLock = null;
        }
    }
}
