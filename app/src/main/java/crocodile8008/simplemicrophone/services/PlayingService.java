package crocodile8008.simplemicrophone.services;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import crocodile8008.simplemicrophone.App;
import crocodile8008.simplemicrophone.R;

/**
 * Created by Andrey Riyk
 */
public class PlayingService extends AbsNotificationService {

    public static void startForNotification() {
        Intent intent = new Intent(getContext(), PlayingService.class);
        getContext().startService(intent);
    }

    public static void stop() {
        Intent intent = new Intent(getContext(), PlayingService.class);
        getContext().stopService(intent);
    }

    @Override
    public void onCreate() {
        startForeground(PLAY_ID,
                makeNotification(R.drawable.notification_play,
                        getContext().getString(R.string.play_notification_title),
                        getContext().getString(R.string.play_notification_subtitle))
        );
    }

}
