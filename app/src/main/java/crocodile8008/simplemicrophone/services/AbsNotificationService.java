package crocodile8008.simplemicrophone.services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import crocodile8008.simplemicrophone.App;
import crocodile8008.simplemicrophone.MainActivity;
import crocodile8008.simplemicrophone.R;

/**
 * Created by Andrey Riyk
 */
public abstract class AbsNotificationService extends Service {

    protected static final int RECORD_ID = 789;
    protected static final int PLAY_ID = 790;

    static Context getContext() {
        return App.getInstance();
    }

    Notification makeNotification(int iconResId, String title, String contentText) {
        Context context = App.getInstance();
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(iconResId)
                        .setContentTitle(title)
                        .setContentText(contentText)
                        .setOngoing(true)
                        .setContentIntent(getPendingIntentToMainActivity());
        return builder.build();
    }

    PendingIntent getPendingIntentToMainActivity() {
        Context context = App.getInstance();
        Intent intent = new Intent(context, MainActivity.class);
        return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
