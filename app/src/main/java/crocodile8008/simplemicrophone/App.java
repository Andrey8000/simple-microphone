package crocodile8008.simplemicrophone;

import android.app.Application;

import crocodile8008.simplemicrophone.services.PlayingService;
import crocodile8008.simplemicrophone.services.RecordService;
import crocodile8008.simplemicrophone.utils.play.RecordPlayer;
import crocodile8008.simplemicrophone.utils.record.Recorder;
import crocodile8008.simplemicrophone.utils.record.RecorderMp3;

/**
 * Created by Andrey Riyk
 */
public class App extends Application {

    private static App instance;

    private Recorder recorder;
    private RecordPlayer recordPlayer;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        prepareExceptionHandler();

        recorder = new RecorderMp3(); // it may be Recorder3GPP
        recordPlayer = new RecordPlayer();
    }

    public Recorder getRecorder() {
        return recorder;
    }

    public RecordPlayer getRecordPlayer() {
        return recordPlayer;
    }

    public static App getInstance() {
        return instance;
    }

    private void prepareExceptionHandler() {
        final Thread.UncaughtExceptionHandler defaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable throwable) {
                throwable.printStackTrace();

                recorder.stopRecording();
                recordPlayer.stopPlaying();
                PlayingService.stop();
                RecordService.stop();

                defaultHandler.uncaughtException(Thread.currentThread(), throwable);
            }
        });
    }

}
