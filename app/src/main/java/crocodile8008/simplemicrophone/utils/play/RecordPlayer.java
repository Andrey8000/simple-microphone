package crocodile8008.simplemicrophone.utils.play;

import android.media.MediaPlayer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.widget.ProgressBar;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import crocodile8008.simplemicrophone.App;
import crocodile8008.simplemicrophone.helpers.Lo;
import crocodile8008.simplemicrophone.helpers.WakeLockHelper;

/**
 * Created by Andrey Riyk
 */
public class RecordPlayer implements ViewProgressUpdater {

    private MediaPlayer mediaPlayer;
    private boolean isPlayingNow;
    private String lastPayedFilePath;
    private final Handler handler = new Handler();
    private final ProgressUpdater progressUpdater = new ProgressUpdater();
    private WeakReference<ProgressBar> weakProgressBar;
    private final List<WeakReference<OnStopListener>> externalOnStopListeners = new ArrayList<>();

    public void startPlaying(String fileName) throws IOException {
        if (isPlayingNow) {
            return;
        }
        mediaPlayer = new MediaPlayer();
        lastPayedFilePath = fileName;
        Lo.i("startPlaying: " + fileName + "  " + new File(fileName).length());
        mediaPlayer.setDataSource(fileName);
        mediaPlayer.prepare();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                stopPlaying();
            }
        });
        mediaPlayer.start();
        isPlayingNow = true;
        startViewProgressUpdating();

        WakeLockHelper.acquire();
    }

    private void notifyStopListeners() {
        for (int i = externalOnStopListeners.size() - 1; i >= 0; i--) {
            OnStopListener listener = externalOnStopListeners.get(i).get();
            if (listener != null) {
                listener.onStop();
                if (listener.isNeedUnsubscribeOnStop()) {
                    externalOnStopListeners.remove(i);
                }
            } else {
                externalOnStopListeners.remove(i);
            }
        }
    }

    public void seekTo(int position) {
        if (isPlayingNow && mediaPlayer != null) {
            mediaPlayer.seekTo(position);
        }
    }

    public void addOnCompletionListener(OnStopListener onCompletionListener) {
        externalOnStopListeners.add(new WeakReference<>(onCompletionListener));
    }

    public void removeOnCompletionListener(OnStopListener onCompletionListener) {
        for (int i = externalOnStopListeners.size() - 1; i >= 0; i--) {
            OnStopListener listener = externalOnStopListeners.get(i).get();
            if (listener != null && listener.equals(onCompletionListener)) {
                externalOnStopListeners.remove(i);
                return;
            }
        }
    }

    public void setViewForProgress(ProgressBar progressBar) {
        if (progressBar == null) {
            weakProgressBar = null;
            return;
        }
        weakProgressBar = new WeakReference<>(progressBar);
    }

    public void stopPlaying() {
        if (!isPlayingNow) {
            return;
        }
        mediaPlayer.release();
        mediaPlayer = null;
        updProgressInAssignedView(1000, 0);
        stopViewProgressUpdating();
        notifyStopListeners();
        isPlayingNow = false;

        WakeLockHelper.release();
    }

    public boolean isPlayingNow() {
        return isPlayingNow;
    }

    @Nullable
    public String getLastPayedFilePath() {
        return lastPayedFilePath;
    }

    private void startViewProgressUpdating() {
        handler.removeCallbacks(progressUpdater);
        int updPeriod = getOptimalProgressUpdPeriod(mediaPlayer.getDuration());
        progressUpdater.setUpdPeriod(updPeriod);
        handler.postDelayed(progressUpdater, updPeriod);
    }

    private void stopViewProgressUpdating() {
        handler.removeCallbacks(progressUpdater);
    }

    @Nullable
    private ProgressBar getViewForProgress() {
        if (weakProgressBar == null) {
            return null;
        }
        return weakProgressBar.get();
    }

    @Override
    public void updProgressInAssignedView(int max, int current) {
        ProgressBar progressBar = getViewForProgress();
        if (progressBar != null) {
            progressBar.setMax(max);
            // for cases when mediaPlayer.getCurrentPosition() return wrong values on fast updating
            int currProgress = progressBar.getProgress();
            if (current > currProgress || current < currProgress - 100) {
                progressBar.setProgress(current);
            }
        }
    }

    private int getOptimalProgressUpdPeriod(int trackDuration) {
        int result = trackDuration / 400;
        if (result < ProgressUpdater.UPD_PERIOD_MIN) {
            result = ProgressUpdater.UPD_PERIOD_MIN;
        } else if (result > ProgressUpdater.UPD_PERIOD_MAX) {
            result = ProgressUpdater.UPD_PERIOD_MAX;
        }
        return result;
    }

    private class ProgressUpdater implements Runnable {

        public static final int UPD_PERIOD_MIN = 25;
        public static final int UPD_PERIOD_DEFAULT = 100;
        public static final int UPD_PERIOD_MAX = 500;

        private int updPeriod = UPD_PERIOD_DEFAULT;

        public void setUpdPeriod(int updPeriod) {
            this.updPeriod = updPeriod;
        }

        @Override
        public void run() {
            if (!isPlayingNow || mediaPlayer == null || !mediaPlayer.isPlaying()) {
                return;
            }
            updProgressInAssignedView(mediaPlayer.getDuration(), mediaPlayer.getCurrentPosition());
            handler.postDelayed(this, updPeriod);
        }

    }

    public interface OnStopListener {

        void onStop();

        boolean isNeedUnsubscribeOnStop();
    }
}
