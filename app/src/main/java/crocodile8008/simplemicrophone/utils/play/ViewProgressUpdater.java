package crocodile8008.simplemicrophone.utils.play;

/**
 * Created by Andrey Riyk
 */
public interface ViewProgressUpdater {

    void updProgressInAssignedView(int max, int current);

}
