package crocodile8008.simplemicrophone.utils;

import android.annotation.SuppressLint;
import android.os.Environment;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import crocodile8008.simplemicrophone.helpers.Lo;

/**
 * Created by Andrey Riyk
 */
@SuppressLint("SimpleDateFormat")
@SuppressWarnings("ResultOfMethodCallIgnored")
public class MicFileUtils {

    public static final String WORK_DIR_PATH = Environment.getExternalStorageDirectory() + "/Simple mic";
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd  HH-mm  ss");
    private static final Comparator<FileContainer> COMPARATOR = new Comparator<FileContainer>() {
        @Override
        public int compare(FileContainer file, FileContainer file2) {
            return file2.getModifyTime() > file.getModifyTime() ? 1 : -1;
        }
    };

    public MicFileUtils() {
        File workDir = new File(WORK_DIR_PATH);
        workDir.mkdirs();
    }

    public String getNewRecordFileName(String fileExtension) {
        return WORK_DIR_PATH + "/" + createFileName() + fileExtension;
    }

    public String getAndCreateNewRecordFileName(String fileExtension) throws IOException {
        String path = getNewRecordFileName(fileExtension);
        File file = new File(path);
        file.createNewFile();
        Lo.v("getAndCreateNewRecordFileName: " + path);
        return path;
    }

    private String createFileName() {
        return DATE_FORMAT.format(new Date());
    }

    public List<FileContainer> getFilesListInWorkDir() {
        File workDir = new File(WORK_DIR_PATH);
        File filesArray[] = workDir.listFiles();
        List<FileContainer> containerList = new ArrayList<>();
        for (File file : filesArray) {
            FileContainer container = new FileContainer(file);
            if (file.exists()) {
                container.setModifyTime(file.lastModified());
            }
            containerList.add(container);
        }
        Collections.sort(containerList, COMPARATOR);
        return containerList;
    }

    public int getFilesCntInWorkDir() {
        File workDir = new File(WORK_DIR_PATH);
        File filesArray[] = workDir.listFiles();
        return filesArray.length;
    }

    public static boolean isFileSound(File file) {
        if (file == null) {
            return false;
        }
        String name = file.getName();
        return name.contains(".3gpp") || name.contains(".mp3");
    }

    /**
     * Wrapper for File with cached modified time
     */
    public static class FileContainer {
        public final File file;
        private long modifyTime;

        public FileContainer(File file) {
            this.file = file;
        }

        public long getModifyTime() {
            return modifyTime;
        }

        public void setModifyTime(long modifyTime) {
            this.modifyTime = modifyTime;
        }

    }
}
