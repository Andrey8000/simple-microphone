package crocodile8008.simplemicrophone.utils.record;

import android.media.MediaRecorder;
import android.support.annotation.Nullable;

import java.io.IOException;

import crocodile8008.simplemicrophone.helpers.WakeLockHelper;
import crocodile8008.simplemicrophone.utils.MicFileUtils;

/**
 * Created by Andrey Riyk
 */
public class Recorder3GPP implements Recorder {

    private MediaRecorder mediaRecorder;
    private final MicFileUtils micFileUtils;
    private boolean isRecordingNow;
    private String lastRecordFilePath;

    public Recorder3GPP() {
        micFileUtils = new MicFileUtils();
    }

    @Override
    public void startRecording() throws IOException {
        if (isRecordingNow) {
            return;
        }
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        lastRecordFilePath = micFileUtils.getAndCreateNewRecordFileName(getFileExtension());
        mediaRecorder.setOutputFile(lastRecordFilePath);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        mediaRecorder.prepare();
        mediaRecorder.start();
        isRecordingNow = true;

        WakeLockHelper.acquire();
    }

    @Override
    public void stopRecording() {
        if (!isRecordingNow || mediaRecorder == null) {
            return;
        }
        mediaRecorder.stop();
        mediaRecorder.release();
        mediaRecorder = null;
        isRecordingNow = false;

        WakeLockHelper.release();
    }

    @Override
    public boolean isRecordingNow() {
        return isRecordingNow;
    }

    @Override
    @Nullable
    public String getLastRecordFilePath() {
        return lastRecordFilePath;
    }

    @Override
    public String getFileExtension() {
        return ".3gpp";
    }

}
