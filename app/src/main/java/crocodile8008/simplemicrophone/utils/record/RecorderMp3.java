package crocodile8008.simplemicrophone.utils.record;

import android.support.annotation.Nullable;

import java.io.IOException;

import com.uraroji.garage.android.mp3recvoice.RecMicToMp3;

import crocodile8008.simplemicrophone.helpers.WakeLockHelper;
import crocodile8008.simplemicrophone.utils.MicFileUtils;

/**
 * Created by Andrey Riyk
 */
public class RecorderMp3 implements Recorder {

    private RecMicToMp3 recMicToMp3;
    private MicFileUtils micFileUtils;
    private boolean isRecordingNow;
    private String lastRecordFilePath;

    public RecorderMp3() {
        micFileUtils = new MicFileUtils();
        //TODO it may be optimized if remove first argument in RecMicToMp3 constructor
        String tmpRecordFilePath = MicFileUtils.WORK_DIR_PATH + "/tmp.mp3";
        recMicToMp3 = new RecMicToMp3(tmpRecordFilePath, 32000);
    }

    @Override
    public void startRecording() throws IOException {
        if (isRecordingNow) {
            return;
        }
        lastRecordFilePath = micFileUtils.getNewRecordFileName(getFileExtension());
        recMicToMp3.setFilePath(lastRecordFilePath);
        recMicToMp3.start();
        isRecordingNow = true;

        WakeLockHelper.acquire();
    }

    @Override
    public void stopRecording() {
        if (!isRecordingNow) {
            return;
        }
        recMicToMp3.stop();
        isRecordingNow = false;

        WakeLockHelper.release();
    }

    @Override
    public boolean isRecordingNow() {
        return isRecordingNow;
    }

    @Nullable
    @Override
    public String getLastRecordFilePath() {
        return lastRecordFilePath;
    }

    @Override
    public String getFileExtension() {
        return ".mp3";
    }
}
