package crocodile8008.simplemicrophone.utils.record;

import java.io.IOException;

/**
 * Created by Andrey Riyk
 */
public interface Recorder {

    void startRecording() throws IOException;

    void stopRecording();

    boolean isRecordingNow();

    String getLastRecordFilePath();

    String getFileExtension();
}
