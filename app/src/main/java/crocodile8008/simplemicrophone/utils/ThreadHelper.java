package crocodile8008.simplemicrophone.utils;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created by Andrey Riyk
 */
public class ThreadHelper {

    public static final Executor COMMON_EXECUTOR_POOL = Executors.newFixedThreadPool(4);

}
