package crocodile8008.simplemicrophone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.io.File;
import java.io.IOException;

import crocodile8008.simplemicrophone.helpers.Lo;
import crocodile8008.simplemicrophone.services.PlayingService;
import crocodile8008.simplemicrophone.services.RecordService;
import crocodile8008.simplemicrophone.utils.play.RecordPlayer;
import crocodile8008.simplemicrophone.utils.record.Recorder;
import crocodile8008.simplemicrophone.view.helpers.DialogHelper;
import crocodile8008.simplemicrophone.view.FilesRecyclerAdapter;
import crocodile8008.simplemicrophone.view.MainFragment;

public class MainActivity extends AbsStateActivity {

    private static final String MAIN_FRAGMENT_TAG = "mainFragment";

    private RecordPlayer recordPlayer;
    private Recorder recorder;
    private MainFragment mainFragment;
    private DialogHelper dialogHelper;

    private AudioManager audioManager;
    private final AudioManager.OnAudioFocusChangeListener audioFocusChangeListener =
            new AudioFocusListener();
    private HeadsetIntentReceiver headsetReceiver;
    private final OnPlayStopListener onPlayStopListener = new OnPlayStopListener();

    private boolean isReattachFragmentDelayed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        Toolbar actionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(actionBarToolbar);

        recorder = App.getInstance().getRecorder();
        recordPlayer = App.getInstance().getRecordPlayer();
        dialogHelper = new DialogHelper(this, recorder, new FilesChangesNotifyCallback());

        setupMainFragment(savedInstanceState);
        setupClickListeners();
        registerHeadsetReceiver();
    }

    private void setupMainFragment(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            mainFragment = new MainFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.homeLay, mainFragment, MAIN_FRAGMENT_TAG)
                    .commit();
        } else {
            mainFragment = (MainFragment) getSupportFragmentManager()
                    .findFragmentByTag(MAIN_FRAGMENT_TAG);
        }
    }

    private void setupClickListeners() {
        mainFragment.setRecordClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startOrStopRecord();
            }
        });

        mainFragment.setFilesListClickListener(new FilesRecyclerAdapter.ItemClickListener() {
            @Override
            public void onClick(File file, int position) {
                handleFileListClick(file);
            }
        });

        mainFragment.setFilesLongClickListener(new FilesRecyclerAdapter.LongClickListener() {
            @Override
            public void onLongClick(File file, int position) {
                dialogHelper.showLongClickChoice(file);
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (isResumedState()) {
            reattachMainFragment();
        } else {
            isReattachFragmentDelayed = true;
        }
    }

    private void reattachMainFragment() {
        getSupportFragmentManager().beginTransaction()
                .detach(mainFragment)
                .attach(mainFragment)
                .commit();
    }

    private void gainAudiFocus() {
        int result = audioManager.requestAudioFocus(audioFocusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        Lo.v("gainAudiFocus result: " + result);
    }

    private void removeAudioFocus() {
        audioManager.abandonAudioFocus(audioFocusChangeListener);
    }

    private void registerHeadsetReceiver() {
        headsetReceiver = new HeadsetIntentReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
        getApplicationContext().registerReceiver(headsetReceiver, intentFilter);
    }

    private void unregisterHeadsetReceiver() {
        getApplicationContext().unregisterReceiver(headsetReceiver);
    }

    private void startOrStopRecord() {
        if (recorder.isRecordingNow()) {
            stopRecordWithRemoveNotification();
            mainFragment.handleFilesInDirChanges();
            showSnackBar(getString(R.string.file_saved) + ": " + recorder.getLastRecordFilePath());
        } else {
            if (recordPlayer.isPlayingNow()) {
                stopPlayWithRemoveNotificationAndAudioFocus();
            }
            try {
                recorder.startRecording();
                RecordService.startForNotification();
            } catch (IOException e) {
                e.printStackTrace();
                showSnackBar("Exception: " + e);
            }
        }
    }

    private void stopRecordWithRemoveNotification() {
        recorder.stopRecording();
        RecordService.stop();
    }

    private void stopPlayWithRemoveNotificationAndAudioFocus() {
        recordPlayer.stopPlaying();
        PlayingService.stop();
        removeAudioFocus();
    }

    private void handleFileListClick(File file) {
        if (recordPlayer.isPlayingNow()) {
            stopPlayWithRemoveNotificationAndAudioFocus();
            boolean isSameFile = recordPlayer.getLastPayedFilePath() != null
                    && recordPlayer.getLastPayedFilePath().equals(file.getAbsolutePath());
            if (isSameFile) {
                return;
            }
        }

        removeAudioFocus();
        gainAudiFocus();
        try {
            recordPlayer.startPlaying(file.getAbsolutePath());
            PlayingService.startForNotification();
            recordPlayer.addOnCompletionListener(onPlayStopListener);
        } catch (IOException e) {
            e.printStackTrace();
            showSnackBar("Exception: " + e);
            removeAudioFocus();
        }
    }

    private void showSnackBar(String text) {
        Snackbar.make(findViewById(android.R.id.content), text, Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isReattachFragmentDelayed) {
            reattachMainFragment();
            isReattachFragmentDelayed = false;
        }
    }

    @Override
    public void onDestroy() {
        unregisterHeadsetReceiver();
        super.onDestroy();
    }

    @Override
    public void onFinish() {
        stopRecordWithRemoveNotification();
        stopPlayWithRemoveNotificationAndAudioFocus();
    }

    //region Classes

    private class HeadsetIntentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(AudioManager.ACTION_AUDIO_BECOMING_NOISY)) {
                stopPlayWithRemoveNotificationAndAudioFocus();
            }
        }
    }

    private class AudioFocusListener implements AudioManager.OnAudioFocusChangeListener {
        @Override
        public void onAudioFocusChange(int focusChange) {
            Lo.v("onAudioFocusChange: " + focusChange);
            if(focusChange <= 0) {
                stopPlayWithRemoveNotificationAndAudioFocus();
            }
        }
    }

    private class FilesChangesNotifyCallback implements DialogHelper.FilesChangesNotify {
        @Override
        public void onFilesChanged() {
            stopPlayWithRemoveNotificationAndAudioFocus();
            if (mainFragment == null) {
                return;
            }
            mainFragment.handleFilesInDirChanges();
        }
    }

    private class OnPlayStopListener implements RecordPlayer.OnStopListener {
        @Override
        public void onStop() {
            PlayingService.stop();
            if (mainFragment != null) {
                mainFragment.redrawAdapterView();
            }
        }

        @Override
        public boolean isNeedUnsubscribeOnStop() {
            return true;
        }
    }

    //endregion
}
